## Extensions gnome-shell utiles

[Extension pour Firefox](https://extensions.gnome.org/local/#)

#### Apparence

  * [Dash to dock](https://extensions.gnome.org/extension/307/dash-to-dock/) (Pensez dans les paramètres à activer l'effet minimiser la fenêtre au clic sur icône)
  * [Dash to Panel](https://extensions.gnome.org/extension/1160/dash-to-panel/) (Utile pour faire un panneau en bas comme Windows 10)
  * [(K)StatusNotifierItem/AppIndicator Support](https://extensions.gnome.org/extension/615/appindicator-support/)  (Pour les icones dans la zone de notification)
  * [Remove Dropdown arrows](https://extensions.gnome.org/extension/800/remove-dropdown-arrows/) (Retire les flèches près des menus)
  * [Top icons Redux](https://extensions.gnome.org/extension/1497/topicons-redux/) (Pour les icones dans la zone de notification)
  * [User theme](https://extensions.gnome.org/extension/19/user-themes/) (Pour pouvoir appliquer des thèmes à gnome)
  * [Desktop Icons](https://extensions.gnome.org/extension/1465/desktop-icons/) (Pour avoir les icônes sur le bureau)
  * [Status Area Horizontal Spacing](https://extensions.gnome.org/extension/355/status-area-horizontal-spacing/) (Pour l'espacement entre les icones)
  * [User Icon Displayed](https://extensions.gnome.org/extension/1885/user-icon-displayed/) (Pour afficher l'icone utilisateur dans le menu système)

#### Menus

  * [Arc Menu](https://extensions.gnome.org/extension/1228/arc-menu/) (Remplace le menu Activités, on peut rajouter une icône, dépendance nécéssaire *sudo apt install gir1.2-gmenu-3.0*)
  * [Places Status Indicator](https://extensions.gnome.org/extension/8/places-status-indicator/) (Rajoute un menu Emplacement pour l'accès direct au dossiers, medias externes...)
  * [Appfolders Management extension ](https://extensions.gnome.org/extension/1217/appfolders-manager/) (Pour grouper des icones du menu de gnome-shell)

### Gestion Fenêtres

  * [Auto move windows](https://extensions.gnome.org/extension/16/auto-move-windows/) (Permet d'attribuer un espace de travail à une application)
  * [Alternate Tab](https://extensions.gnome.org/extension/15/alternatetab/) (Alt tab alternatif)
  * [Transparent Window Moving](https://extensions.gnome.org/extension/1446/transparent-window-moving/) (Rend les fenêtres transparentes quand on les déplacent)

#### Monitoring / Indicateurs

  * [Apt Update Indicator](https://extensions.gnome.org/extension/1139/apt-update-indicator/) (Pour afficher les mises à jour disponibles)
  * [Clipboard Indicator](https://extensions.gnome.org/extension/779/clipboard-indicator/) (Indicateur pour les copier-coller)
  * [Lock Keys](https://extensions.gnome.org/extension/36/lock-keys/) (Pour afficher un indicateur pour le vérouillage majuscules et pavé numérique)
  * [Openweather](https://extensions.gnome.org/extension/750/openweather/) (Pour afficher la méteo)

#### Utile

  * [Draw on Your Screen](https://extensions.gnome.org/extension/1683/draw-on-you-screen/) (Pour dessiner sur l'écran)
  * [Do not disturb](https://extensions.gnome.org/extension/1480/do-not-disturb/) (Pour avoir un bouton "ne pas déranger" bloquant les notifications)
  * [Drop down terminal X](https://extensions.gnome.org/extension/1509/drop-down-terminal-x/) (Terminal à la quake)
  * [GSConnect](https://extensions.gnome.org/extension/1319/gsconnect/) (Pour relier gnome-shell à son téléphone android)
  * [Internet Radio](https://extensions.gnome.org/extension/836/internet-radio/) (Pour l'écoute de stations radio)
  * [IP Finder](https://extensions.gnome.org/extension/1190/ip-finder/) (Affiche IP, drapeau...etc)
  * [Vitals](https://extensions.gnome.org/extension/1460/vitals/) (Affiche température, ram, ventilos...)
  * [Tweaks in System Menu](https://extensions.gnome.org/extension/1653/tweaks-in-system-menu/) (Pour avoir une icone vers gnome-tweaks dans le menu système)






