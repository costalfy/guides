# Groupes Telegram (Discussions)

## Distributions Linux

  * [Ubuntu-Fr](https://t.me/ubuntuFr)
  * [Ubuntu-Fr Support](https://t.me/ubuntufrsupport)
  * [Elementary OS USers](https://t.me/elementaryosusers)
  * [Pop! OS Fr](https://t.me/PopOs_fr)
  * [Fedora Fr](https://t.me/fedora_fr)
  * [Fedora](https://t.me/fedora)
  * [Solus Fr](https://t.me/solus_fr)
  * [Solus Unofficial Discussion](https://t.me/solusproject)
  * [Archlinux Fr](https://t.me/Archlinux_Fr)
  * [Archlinux](https://t.me/archlinuxgroup)
  * [Debian Fr](https://t.me/Debian_Fr)
  * [Debian GNU/Linux](https://t.me/DebianProject)
  * [Deepin Fr](https://t.me/deepin_fr)
  * [Deepin GNU/Linux](https://t.me/deepin)
  * [Voidlinux Fr](https://t.me/voidlinux_fr)
  * [KDE Neon users](https://t.me/kdeneon)
  * [Calculate Linux Fr](https://t.me/joinchat/BxLzSUMTtRIgZK4PGpfDaQ)

## Discussions linuxiennes, opensource, libre...

  * [Club Qwant](https://t.me/ClubQwant)
  * [Linux Fr](https://t.me/Linux_Fr)
  * [Actualia](https://t.me/joinchat/HxD0URdpsu5kopa1ltT1zg)
  * [ArchLinux Challenge](https://t.me/archchallenge)
  * [System76 Fan Club](https://t.me/system76_chat)

## Jeux

  * [Linux Gaming](https://t.me/linux_gaming)
  * [Linux Gaming FR](https://t.me/linuxgaming_fr)
  * [Humble Bundle](https://t.me/humblebundle)

## Android

  * [Android Fr](https://t.me/Android_Fr)
  * [Blokada Francophone](https://t.me/blokadafr)
  * [/e/ Community Support](https://t.me/joinchat/C49jqEUbP-CNUGjzu-dTEA)
  * [/e/ public (Français)](https://t.me/joinchat/AdMoEEzIWQrD8ESmMKnMSQ)
  * [Fennec F-Droid](https://t.me/fennec_fdroid)

# Channels Telegram (Annonces)

## Telegram

  * [Telegram](https://t.me/telegram)
  * [Astuces Telegram](https://t.me/AstucesTelegram)

## Linux et Logiciels Libres

  * [Linuxgram](https://t.me/linuxgram)
  * [Linuxfr.org](https://t.me/LinuxFrOrg)
  * [Ubuntu Fr](https://t.me/ubuntufrannonce)
  * [Fedora News](https://t.me/fedoranews)
  * [Archlinux Fr](https://t.me/ArchlinuxFr)
  * [Logiciels Libres](https://t.me/logicielslibres)
  * [System76](https://t.me/system76_news)
  * [Liberapay](https://t.me/liberapay)

## Développement

  * [Journal du hacker](https://t.me/journalduhacker)
  * [Spring releases](https://t.me/SpringReleases)
  * [Angular Blog](https://t.me/AngularBlog)

## Jeux

  * [Linux Gaming & SteamOS News](https://t.me/linuxgaming)
  * [Humble Bundle](https://t.me/humblebundle)

## Android

  * [/e/ Announcements](https://t.me/mydataismydata)
  * [Libreware](https://t.me/libreware)
  * [No Gapps](https://t.me/NoGapps)
  * [No Goolag](https://t.me/NoGoolag)

## Hacking

  * [fs0c131y](https://t.me/fs0c131yOfficialChannel)
  * [Kali Linux & Hacking](https://t.me/kaliLinuxandhacking)

## VPN

  * [ProtonVPN](https://t.me/ProtonVPN_com)

## Politique

  * [François Ruffin](https://t.me/francoisruffin)

## Musique en streaming

  * [Music Streaming](https://t.me/Music_Streaming)

## Nouvelles du monde

  * [RTBF Info](https://t.me/rtbfinfo)
