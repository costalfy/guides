# Guide Post-Installation Ubuntu 18.10

![Capture](https://framagit.org/pixtolero/guides/raw/master/screenshotubuntu1810.png)

## Prérequis

  * Installer Ubuntu 18.10 avec l'option installation minimale durant l'installation pour ne pas se retrouver avec des logiciels superflus
  * Activer la connexion internet pour l'installation et cocher la case pour installer les pilotes et codecs


## Mise à jour du système

    sudo apt update && sudo apt upgrade

## GNOME

### Logiciel _Ajustements_

    sudo apt install gnome-tweak-tool

  * Retour du clic droit, dans la partie Clavier et Souris, cocher "Surface" en bas
  * Pour les portable, il y a l'option pour afficher le pourcentage de la batterie

### Extensions gnome-shell indispensables

    sudo apt install chrome-gnome-shell

  * [Extension pour firefox](https://addons.mozilla.org/en-US/firefox/addon/gnome-shell-integration/)
  * [Dash to dock](https://extensions.gnome.org/extension/307/dash-to-dock/) (pensez dans les paramètres à activer l'effet minimiser la fenêtre au clic sur icône)
  * [(K)StatusNotifierItem/AppIndicator Support](https://extensions.gnome.org/extension/615/appindicator-support/)
  * [Alternate Tab](https://extensions.gnome.org/extension/15/alternatetab/) ou [Coverflow alt tab](https://extensions.gnome.org/extension/97/coverflow-alt-tab/)
  * [Appfolders Management extension ](https://extensions.gnome.org/extension/1217/appfolders-manager/)
  * [Applications-menu](https://extensions.gnome.org/extension/6/applications-menu/)
  * [Apt Update indicator](https://extensions.gnome.org/extension/1139/apt-update-indicator/)
  * [Auto move windows](https://extensions.gnome.org/extension/16/auto-move-windows/)
  * [Clipboard Indicator](https://extensions.gnome.org/extension/779/clipboard-indicator/)
  * [Do not disturb](https://extensions.gnome.org/extension/1480/do-not-disturb/)
  * [Drop down terminal X](https://extensions.gnome.org/extension/1509/drop-down-terminal-x/)
  * [Extension Update Notifier](https://extensions.gnome.org/extension/1166/extension-update-notifier/)
  * [GSConnect](https://extensions.gnome.org/extension/1319/gsconnect/)
  * [Impatience](https://extensions.gnome.org/extension/277/impatience/)
  * [Internet Radio](https://extensions.gnome.org/extension/836/internet-radio/)
  * [IP Finder](https://extensions.gnome.org/extension/1190/ip-finder/)
  * [Lock Keys](https://extensions.gnome.org/extension/36/lock-keys/)
  * [Media player indicator](https://extensions.gnome.org/extension/55/media-player-indicator/)
  * [Openweather](https://extensions.gnome.org/extension/750/openweather/)
  * [Places Menu indicator](https://extensions.gnome.org/extension/8/places-status-indicator/)
  * [Random Wallpaper](https://extensions.gnome.org/extension/1040/random-wallpaper/)
  * [Refresh Wifi Connections](https://extensions.gnome.org/extension/905/refresh-wifi-connections/)
  * [Removable drive menu](https://extensions.gnome.org/extension/7/removable-drive-menu/)
  * [Remove Dropdown arrows](https://extensions.gnome.org/extension/800/remove-dropdown-arrows/)
  * [Sound Output Device Chooser](https://extensions.gnome.org/extension/906/sound-output-device-chooser/)
  * [Top icons Plus](https://extensions.gnome.org/extension/1031/topicons/)
  * [User theme](https://extensions.gnome.org/extension/19/user-themes/)
  * [Vitals](https://extensions.gnome.org/extension/1460/vitals/)

#### Prime-indicator

    wget https://raw.githubusercontent.com/fffilo/prime-indicator/master/install.sh -O - | sh

### Raccourcis GNOME à savoir

  * **Touche Super (windows)** _Ouvre / Ferme le menu Activités_
  * **Alt + F2** _Prompt pour commande_
  * **Super + A** _Ouvre le parcoureur d'applications_
  * **Super + Tab** _Selectionne tour à tour les fenêtres puvertes (comme ALt Tab)_
  * **Super + `** _Selectionne tour à tour les fenêtes d'une *même* application_
  * **Ctrl + Alt + flèche (bas ou haut)** _Se dpélacer entre les espaces de travail_
  * **Super + M** _Ouvre le menu des notifications_
  * **Super + L** _Vérouille l'écran_
  * **Super + Shift + flèche (droite ou gauche)** _Déplace la fenêtre de l'effet focus sur le moniteur suivant_
  * **Super + flèche (droite ou gauche)** _Effet focus / ou annulation du focus de la fenêtre sur la droite ou la gauche_
  * **Super + flèche (bas ou haut)** _Maximise ou minimise la fenêtre sous effet focus_
  * **Super + Q** _Quitte l'application en focus_
  * **Super + F10** _Ouvre le menu de l'application sous focus_
  * **Super + H** _Cache l'application sous focus_
  * **Ctrl+Alt+T** _Ouvre un terminal_
  * **Super + D** _Affiche le bureau_
  * **Super + Espace** _CHanger la langue du clavier_
  * **Ctrl+Alt+Del** _Se déloguer_
  * **Super + Shift + Page (Up ou Down)** _Déplace la fenêtre vers l'espace de travail plus haut ou plus bas_

## Logiciels à installer

### Codecs

    sudo apt install ubuntu-restricted-extras

### Suite bureautique

    sudo add-apt-repository ppa:libreoffice/ppa
    sudo apt update
    sudo apt install libreoffice libreoffice-l10n-fr

### Pour la Photo

    sudo add-apt-repository ppa:otto-kesselgulasch/gimp
    sudo apt update
    sudo apt install gimp gmic
    sudo apt install darktable shotwell rapid-photo-downloader

#### Version développement de darktable

    sudo sh -c "echo 'deb http://download.opensuse.org/repositories/graphics:/darktable:/master/xUbuntu_18.10/ /' > /etc/apt/sources.list.d/graphics:darktable:master.list"
    wget -nv https://download.opensuse.org/repositories/graphics:darktable:master/xUbuntu_18.10/Release.key -O Release.key
    sudo apt update && sudo aptinstall darktable

### Pour la musique et la vidéo

    sudo apt remove rhythmbox
    sudo apt remove mpv && sudo apt install totem
    sudo add-apt-repository ppa:gnumdk/lollypop
    sudo apt update
    sudo apt install lollypop

### Internet

    sudo apt install transmission filezilla youtube-dl uget grsync liferea geary

#### protonvpn-cli

    sudo apt install resolvconf
    sudo wget -O protonvpn-cli.sh https://raw.githubusercontent.com/ProtonVPN/protonvpn-cli/master/protonvpn-cli.sh
    sudo chmod +x protonvpn-cli.sh
    sudo ./protonvpn-cli.sh --install
    sudo protonvpn-cli -init

### Applications web

  * Bitwarden, paquet deb [ici](https://github.com/bitwarden/desktop/releases)
  * Mattermost-desktop, paquet deb [ici](https://github.com/mattermost/desktop/releases)
  * Protonmail-bridge, paquet deb [ici](https://protonmail.com/download/protonmail-bridge_1.1.3-1_amd64.deb)
  * DuckieTV, paquet deb [ici](https://github.com/DuckieTV/Nightlies/releases)
  * Molotov, appimage [ici](https://www.molotov.tv/download)
  * Stremio, paquet deb [ici](https://www.stremio.com/downloads)

#### Telegram

    sudo add-apt-repository ppa:atareao/telegram
    sudo apt update
    sudo apt install telegram-desktop

### Qarte

    sudo add-apt-repository ppa:vincent-vandevyvre/vvv
    sudo apt update
    sudo apt install qarte

#### minitube

    sudo apt install minitube

### Clouds

  * Cozy, appimage [ici](https://github.com/cozy-labs/cozy-desktop/releases)
  * Megasync, paquet deb [ici](https://mega.nz/linux/MEGAsync/xUbuntu_18.10/amd64/megasync-xUbuntu_18.10_amd64.deb)
  * Nautilus-Megasync, paquet deb [ici](https://mega.nz/linux/MEGAsync/xUbuntu_18.10/amd64/nautilus-megasync-xUbuntu_18.10_amd64.deb)

#### Nextcloud

    sudo add-apt-repository ppa:nextcloud-devs/client
    sudo apt update
    sudo apt install nextcloud-client

#### Dropbox

    sudo apt install nautilus-dropbox

### Jeux

    sudo apt install steam

#### Gamehub

    sudo add-apt-repository ppa:tkashkin/gamehub
    sudo apt update
    sudo apt install com.github.tkashkin.gamehub

### Divers

    sudo apt install calibre baobab testdisk eog eog-plugins gnome-calendar gufw deja-dup caffeine cheese vokoscreen neofetch gnome-maps gnome-weather gnome-calculator  gnome-clocks simple-scan gpodder gnome-documents gnome-sushi gnome-contacts virtualbox virtualbox-ext-pack bleachbit memtest86+

  * multibootusb, paquet deb [ici](http://multibootusb.org/page_download/)
  * qomui, paquet deb [ici](https://github.com/corrad1nho/qomui/releases)

### TOR

    sudo apt install torbrowser-launcher onioncircuits

### onionshare (partage de fichiers)

    sudo add-apt-repository ppa:micahflee/ppa
    sudo apt install -y onionshare

### Virtualbox 6

Prérequis:

    sudo apt update
    sudo apt-get install gcc make linux-headers-$(uname -r) dkms

Rajout du dépôt:

    wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
    wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
    sudo sh -c 'echo "deb http://download.virtualbox.org/virtualbox/debian $(lsb_release -sc) contrib" >> /etc/apt/sources.list.d/virtualbox.list'

On retire l'ancienne version:

    sudo apt remove virtualbox virtualbox-5.2

Installation:

    sudo apt update
    sudo apt-get install virtualbox-6.0

## Optimisation

### Preload

    sudo apt install preload

### TLP pour la batterie

    sudo apt install tlp tlp-rdw
    sudo tlp start

application graphique:

    sudo add-apt-repository ppa:linuxuprising/apps
    sudo apt update && sudo apt install tlpui

### Nettoyage cache paquets

    sudo apt clean
    sudo apt autoremove

### Menu du Grub

    sudo nano /etc/default/grub

commenter **GRUB_TIMEOUT_STYLE=hidden**

Puis mettez 5 (nombre de secondes) au lieu de 0 à **GRUB_TIMEOUT=**


### Kernel Liquorix

    sudo add-apt-repository ppa:damentz/liquorix && sudo apt-get update
    sudo apt-get install linux-image-liquorix-amd64 linux-headers-liquorix-amd64

[site](https://liquorix.net/)

### Kernel Xanmod

    echo 'deb http://deb.xanmod.org releases main' | sudo tee /etc/apt/sources.list.d/xanmod-kernel.list && wget -qO - https://dl.xanmod.org/gpg.key | sudo apt-key add -
    sudo apt update && sudo apt install linux-xanmod

[site](https://xanmod.org/)

### UUKU (Ubuntu Update Kernel Utility)

    sudo apt-add-repository -y ppa:teejee2008/ppa
    sudo apt-get update
    sudo apt-get install ukuu

ukuu vous permet de gérer facilement l'installation et la désinstallation de kernels linux. Sur le même dépôt il y a des paquets pouvant être utile comme **selene**, **polo-file-manager** et **aptik**.

### Driver NVIDIA spécfique

    sudo add-apt-repository ppa:graphics-drivers/ppa
    sudo apt-get update

drivers dispo sur le dépôt:

  * nvidia-driver-304 (anciennes cartes NV4x et G7x)
  * nvidia-driver-340 (anciennes cartes G8x, G9x et GT2xx)
  * nvidia-driver-390 (carte GF1xx)
  * **nvidia-driver-396** (beta)

    sudo apt install libnvidia-cfg1-396 libnvidia-gl-396 nvidia-dkms-396 nvidia-driver-396 nvidia-utils-396 xserver-xorg-video-nvidia-396

  * **nvidia-driver-410** (actuel longue durée)
  * **nvidia-driver-415**

nvidiux, logiciel d'overclocking:

    sudo add-apt-repository ppa:nvidiux/nvidiux
    sudo apt-get update && sudo apt-get install nvidiux

### PPA Oibaf

Pour avoir des versions récentes de Mesa, openCL, openGL...

     sudo add-apt-repository ppa:oibaf/graphics-drivers
     sudo apt-get update

### PPA Pop!OS

    sudo add-apt-repository ppa:system76/pop
    sudo apt-get update

paquets utiles: 

  * **apt-fast**
  * **gnome-podcasts**
  * **gnome-shell-extension-pop-battery-icon-fix**
  * **gnome-shell-extension-pop-suspend-button**
  * **gnome-shell-extension-do-not-disturb**

Pour avoir le thème POP!

    sudo apt install pop-theme pop-wallpapers

Ensuite pour les polices:

  * **Window Titles:** Fira Sans SemiBold 10
  * **Interface:** Fira Sans Book 10
  * **Documents:** Roboto Slab Regular 11
  * **Monospace:** Fira Mono Regular 11

### Oh my ZSH

    sudo apt install git-core zsh
    cd /tmp
    sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    sudo apt install fonts-powerline zsh-syntax-highlighting

Plugins en plus

    cd ~/.oh-my-zsh/custom/plugins
    git clone https://github.com/zsh-users/zsh-autosuggestions

Éditer votre config (plugins, thème):

    nano ~/.zshrc

[Liste des thèmes](https://github.com/robbyrussell/oh-my-zsh/wiki/Themes)
[Liste des plugins](https://github.com/robbyrussell/oh-my-zsh/wiki/Plugins)

Thème Powerline9K

    sudo apt install zsh-theme-powerlevel9k

Puis mettre **ZSH_THEME="powerlevel9k/powerlevel9k"** à ~/.zshrc


Mettre zsh par défaut:

    chsh -s $(which zsh)

[Site](ohmyz.sh)

### LSD (meilleure que la commande ls)

Installer le paquet deb se trouvant [ici](https://github.com/Peltoche/lsd/releases)

Puis ajouter à ~/.bashrc ou ~/.zshrc

    alias ls='lsd'

Polices utiles pour les icônes:

    sudo apt install fonts-powerline fonts-font-awesome

et nerd fonts:

    cd /tmp
    git clone https://github.com/ryanoasis/nerd-fonts.git
    cd nerd-fonts
    ./install.sh

## A savoir

### Snaps

Privilégier les paquets deb aux paquets snaps. Sur l'Ubuntu Sowftware, regardez bien la fiche de l'application pour savoir si c'est une version en paquet ou snap. Le plus sûr reste de **faire ses installation de paquet en terminal via la commande apt**. Au passage, certains snaps sont pré-installés, il faut les retirer comme ceci:

    sudo snap remove gnome-calculator gnome-characters gnome-logs gnome-system-monitor

Ensuite, on installe leur version en paquet deb:

     sudo apt install gnome-calculator gnome-characters gnome-logs gnome-system-monitor

Et enfin, on supprime tout snap et ses composants

     sudo apt purge snapd squashfs-tools gnome-software-plugin-snap

### Virtualbox

Soucis avec modprobe vboxdrv ?

    sudo apt update
    sudo apt install --reinstall linux-headers-$(uname -r) virtualbox-dkms dkms
    sudo modprobe vboxdrv

### Retirer anciens kernels

    dpkg -l | tail -n +6 | grep -E 'linux-image-[0-9]+' | grep -Fv $(uname -r)

Cette commande liste les kernels disponibles avec les mentions suivantes:

  * *rc*: déjà retiré
  * *ii*: installé mais peut être retiré
  * *iU*: A ne pas retirer, prévu dans les mises à jour

Ensuite retirer le kernel avec *sudo apt remove*

### Outils Khali Linux

  * script dispo [ici](https://github.com/LionSec/katoolin)

### DNS Blocklist

  * [wiki de SebSauvage](https://sebsauvage.net/wiki/doku.php?id=dns-blocklist)

