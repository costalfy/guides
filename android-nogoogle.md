# Guide Android No Google

## crDroid + NanoDroid

Téléchager la bonne version de crDroid [ici](https://crdroid.net/dl.php)

Pour Nanodroid, télécharger la dernière version [ici](https://androidfilehost.com/?w=files&flid=198483).

Il faut **NanoDroid-microG**, **NanoDroid-fdroid** et **NanoDroid-patcher**.

## Dépôts à ajouter (ou activer) aux paramètres de f-droid

    microG F-Droid repo

    https://microg.org/fdroid/repo

    microG F-Droid archive

    https://microg.org/fdroid/archive

    IzzySoft repo

    https://apt.izzysoft.de/fdroid/repo

    Guardian Project Repo

    https://guardianproject.info/fdroid/repo


## Applications indispensables

[OpenWeatherMap Provider](https://www.apkmirror.com/apk/lineageos/openweathermap/openweathermap-1-0-release/openweathermap-1-0-android-apk-download/)

À Récupérer sur les dépôts:


|Application    |Description|
|---------------|--------|
|Amaze          |Navigation de fichiers|
|AnySoftKeyboard|Clavier alternatif, pensez à prendre les modules de langues|
|Etar           |Calendrier, mieux que celui installé par défaut|
|NewPipe        |Voir vidéos youtube en mode privé|
|oandbackup     |Sauvegarde et restauration applications+données|
|Odyssey        |Lecture musique|
|Open Camera    |Application photo|
|OpenLauncher   |Launcher|
|QKSMS          |Application SMS|
|VPN Hotspot    |Permet de partager son VPN au travers un partage de connexion|
|Ameixa icons   |Pack d'îcones|
|AntennaPod     |Application pour podcasts|
|Anuto TD       |Jeu de Tower Defense|
|VLC            |Lecteur Vidéo|
|orbot          |Nécéssaire pour utiliser orfox|
|orfox          |Navigateur avec TOR intégré|
|bitwarden      |Client pour le gestionnaire de mots de passes Bitwarden|
|checkers       |Jeu d'echecs|
|cpu info       |Pour avoir des infos sur le CPU et le matériel|
|crosswords     |Jeu de scrabble|
|dandelion*     |Client pour diaspora|
|DAVx5          |Pour synchroniser son calendrier|
|Firefox Klar   |Firefox en mode incognito|
|Feel           |Application pour le sport|
|Fennec F-droid |Navigateur Web|
|Frozen Bubble  |Jeu|
|Gadgetbridge   |Application pour connecter une montre connectée|
|ICSx5          |Pour rajouter un calendrier au format ICS|
|KDEconnect     |Pour connecter le téléphone à un PC sous linux|
|LeMonde.fr     |Pour avoir les grands titres du Monde.fr|
|Libretrivia    |Jeu de trivial pursuit|
|Maps           |Application de cartographie OpenstreetMap hors-ligne|
|Fedilab        |Application pour Mastodon/Peertube|
|Mattermost     |Application pour Mattermost|
|MuPDF Mini     |Visionneuse de documents|
|Mysplash       |Client pour unsplash|
|Nextcloud      |Client NextcloudS|
|Notes          |Prise de notes via serveur Nextcloud|
|Open Note Scan |Pour scanner des notes|
|Openl          |Application de traduction via Deepl|
|openScale      |Application prise de poids|
|OSMAnd~        |Navigation GPS|
|Peertube       |Client peertube|
|RadioDroid     |Application pour streaming radio|
|RedReader      |Client pour reddit|
|Riot.im        |Client pour riot.im|
|SecScanQR      |Pour scanner les QR codes|
|SeriesGuide    |Pour suivre ses séries TV|
|Open Sudoku         |Jeu de sudoku|
|SuperTuxKart   |Jeu de course|
|Telegram FOSS  |Telegram|
|Tiny Tiny RSS  |CLient pour tinytinyRSS|
|Transportr     |Application pour les transports en commun|
|Turo           |Jeu|
|Tutanota       |Application pour mail Tutanota|
|Units          |Pour la convrsion d'unités|
|Weather Forecasts|Prévisions Météo|
|WIFIAnalyzer   |Analyse les résaux wifi aux alentours|


  * Yalp Store
  * Aurora Store

## Blocage DNS

Il faut installer [Adaway](https://f-droid.org/packages/org.adaway/). Il faut rooter le téléphone pour l'utiliser, pour ca il faut soit installer [le zip de LineageOS](https://lineageos.mirrorhub.io/su/) ou l'application [Magisk](https://magiskmanager.com/).

Dans Adaway, on retire tout les URLs de fichier et on ajoute celui de SebSauvage:

    https://sebsauvage.net/hosts/hosts

## Plus

Consulter le [canal Telegram Libreware](http://t.me/Libreware) pour plus d'APK utiles
